vcfR Geneflow v2 application
================================

Version: 0.3

This is a GeneFlow app that helps vcf file visualization

Inputs
------

1. input: VCF file to visualize
2. input: fasta reference file

Parameters
----------

1. min_qual: Minimum Varient Quality
2. min_DP: Minimum read depth in the output graph 
3. max_DP: Maximum read depth in the output graph 
4. min_MQ: Minimum mapping quality
5. max_MQ: Maximum mapping quality 
6. output: A directory that contains the PDF output
