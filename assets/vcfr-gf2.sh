#!/bin/bash

# VCF data visualization in R wrapper script


###############################################################################
#### Helper Functions ####
###############################################################################

## ****************************************************************************
## Usage description should match command line arguments defined below
usage () {
    echo "Usage: $(basename "$0")"
    echo "  --input_vcf => VCF file"
    echo "  --input_fa => FASTA file"
    echo "  --output => Output Folder"
    echo "  --min_qual => Minimum Variant Quality"
    echo "  --min_DP => Minimum Read Depth"
    echo "  --max_DP => Maximum Read Depth"
    echo "  --min_MQ => Minimum Mapping Quality"
    echo "  --max_MQ => Maximum Mapping Quality"
    echo "  --exec_method => Execution method (singularity, auto)"
    echo "  --exec_init => Execution initialization command(s)"
    echo "  --help => Display this help message"
}
## ****************************************************************************

# report error code for command
safeRunCommand() {
    cmd="$@"
    eval "$cmd; "'PIPESTAT=("${PIPESTATUS[@]}")'
    for i in ${!PIPESTAT[@]}; do
        if [ ${PIPESTAT[$i]} -ne 0 ]; then
            echo "Error when executing command #${i}: '${cmd}'"
            exit ${PIPESTAT[$i]}
        fi
    done
}

# print message and exit
fail() {
    msg="$@"
    echo "${msg}"
    usage
    exit 1
}

# always report exit code
reportExit() {
    rv=$?
    echo "Exit code: ${rv}"
    exit $rv
}

trap "reportExit" EXIT

# check if string contains another string
contains() {
    string="$1"
    substring="$2"

    if test "${string#*$substring}" != "$string"; then
        return 0    # $substring is not in $string
    else
        return 1    # $substring is in $string
    fi
}



###############################################################################
## SCRIPT_DIR: directory of current script, depends on execution
## environment, which may be detectable using environment variables
###############################################################################
if [ -z "${AGAVE_JOB_ID}" ]; then
    # not an agave job
    SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
else
    echo "Agave job detected"
    SCRIPT_DIR=$(pwd)
fi
## ****************************************************************************



###############################################################################
#### Parse Command-Line Arguments ####
###############################################################################

getopt --test > /dev/null
if [ $? -ne 4 ]; then
    echo "`getopt --test` failed in this environment."
    exit 1
fi

## ****************************************************************************
## Command line options should match usage description
OPTIONS=
LONGOPTIONS=help,exec_method:,exec_init:,input_vcf:,input_fa:,output:,min_qual:,min_DP:,max_DP:,min_MQ:,max_MQ:,
## ****************************************************************************

# -temporarily store output to be able to check for errors
# -e.g. use "--options" parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(\
    getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@"\
)
if [ $? -ne 0 ]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    usage
    exit 2
fi

# read getopt's output this way to handle the quoting right:
eval set -- "$PARSED"

## ****************************************************************************
## Set any defaults for command line options
EXEC_METHOD="auto"
EXEC_INIT=":"
## ****************************************************************************

## ****************************************************************************
## Handle each command line option. Lower-case variables, e.g., ${file}, only
## exist if they are set as environment variables before script execution.
## Environment variables are used by Agave. If the environment variable is not
## set, the Upper-case variable, e.g., ${FILE}, is assigned from the command
## line parameter.
while true; do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --input_vcf)
            if [ -z "${input_vcf}" ]; then
                INPUT_VCF=$2
            else
                INPUT_VCF=${input_vcf}
            fi
            shift 2
            ;;
        --input_fa)
            if [ -z "${input_fa}" ]; then
                INPUT_FA=$2
            else
                INPUT_FA=${input_fa}
            fi
            shift 2
            ;;
        --output)
            if [ -z "${output}" ]; then
                OUTPUT=$2
            else
                OUTPUT=${output}
            fi
            shift 2
            ;;
        --min_qual)
            if [ -z "${min_qual}" ]; then
                MIN_QUAL=$2
            else
                MIN_QUAL=${min_qual}
            fi
            shift 2
            ;;
        --min_DP)
            if [ -z "${min_DP}" ]; then
                MIN_DP=$2
            else
                MIN_DP=${min_DP}
            fi
            shift 2
            ;;
        --max_DP)
            if [ -z "${max_DP}" ]; then
                MAX_DP=$2
            else
                MAX_DP=${max_DP}
            fi
            shift 2
            ;;
        --min_MQ)
            if [ -z "${min_MQ}" ]; then
                MIN_MQ=$2
            else
                MIN_MQ=${min_MQ}
            fi
            shift 2
            ;;
        --max_MQ)
            if [ -z "${max_MQ}" ]; then
                MAX_MQ=$2
            else
                MAX_MQ=${max_MQ}
            fi
            shift 2
            ;;
        --exec_method)
            if [ -z "${exec_method}" ]; then
                EXEC_METHOD=$2
            else
                EXEC_METHOD=${exec_method}
            fi
            shift 2
            ;;
        --exec_init)
            if [ -z "${exec_init}" ]; then
                EXEC_INIT=$2
            else
                EXEC_INIT=${exec_init}
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option"
            usage
            exit 3
            ;;
    esac
done
## ****************************************************************************

## ****************************************************************************
## Log any variables passed as inputs
echo "Input_vcf: ${INPUT_VCF}"
echo "Input_fa: ${INPUT_FA}"
echo "Output: ${OUTPUT}"
echo "Min_qual: ${MIN_QUAL}"
echo "Min_dp: ${MIN_DP}"
echo "Max_dp: ${MAX_DP}"
echo "Min_mq: ${MIN_MQ}"
echo "Max_mq: ${MAX_MQ}"
echo "Execution Method: ${EXEC_METHOD}"
echo "Execution Initialization: ${EXEC_INIT}"
## ****************************************************************************



###############################################################################
#### Validate and Set Variables ####
###############################################################################

## ****************************************************************************
## Add app-specific logic for handling and parsing inputs and parameters

# INPUT_VCF input

if [ -z "${INPUT_VCF}" ]; then
    echo "VCF file required"
    echo
    usage
    exit 1
fi
# make sure INPUT_VCF is staged
count=0
while [ ! -f "${INPUT_VCF}" ]
do
    echo "${INPUT_VCF} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -f "${INPUT_VCF}" ]; then
    echo "VCF file not found: ${INPUT_VCF}"
    exit 1
fi
INPUT_VCF_FULL=$(readlink -f "${INPUT_VCF}")
INPUT_VCF_DIR=$(dirname "${INPUT_VCF_FULL}")
INPUT_VCF_BASE=$(basename "${INPUT_VCF_FULL}")


# INPUT_FA input

if [ -z "${INPUT_FA}" ]; then
    echo "FASTA file required"
    echo
    usage
    exit 1
fi
# make sure INPUT_FA is staged
count=0
while [ ! -e "${INPUT_FA}" ]
do
    echo "${INPUT_FA} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -e "${INPUT_FA}" ]; then
    echo "FASTA file not found: ${INPUT_FA}"
    exit 1
fi
INPUT_FA_FULL=$(readlink -f "${INPUT_FA}")
INPUT_FA_DIR=$(dirname "${INPUT_FA_FULL}")
INPUT_FA_BASE=$(basename "${INPUT_FA_FULL}")



# OUTPUT parameter
if [ -n "${OUTPUT}" ]; then
    :
    OUTPUT_FULL=$(readlink -f "${OUTPUT}")
    OUTPUT_DIR=$(dirname "${OUTPUT_FULL}")
    OUTPUT_BASE=$(basename "${OUTPUT_FULL}")
    LOG_FULL="${OUTPUT_DIR}/_log"
    TMP_FULL="${OUTPUT_DIR}/_tmp"
else
    :
    echo "Output Folder required"
    echo
    usage
    exit 1
fi


# MIN_QUAL parameter
if [ -n "${MIN_QUAL}" ]; then
    :
else
    :
fi


# MIN_DP parameter
if [ -n "${MIN_DP}" ]; then
    :
else
    :
fi


# MAX_DP parameter
if [ -n "${MAX_DP}" ]; then
    :
else
    :
fi


# MIN_MQ parameter
if [ -n "${MIN_MQ}" ]; then
    :
else
    :
fi


# MAX_MQ parameter
if [ -n "${MAX_MQ}" ]; then
    :
else
    :
fi

## ****************************************************************************

## EXEC_METHOD: execution method
## Suggested possible options:
##   auto: automatically determine execution method
##   singularity: singularity image packaged with the app
##   docker: docker containers from docker-hub
##   environment: binaries available in environment path

## ****************************************************************************
## List supported execution methods for this app (space delimited)
exec_methods="singularity auto"
## ****************************************************************************

## ****************************************************************************
# make sure the specified execution method is included in list
if ! contains " ${exec_methods} " " ${EXEC_METHOD} "; then
    echo "Invalid execution method: ${EXEC_METHOD}"
    echo
    usage
    exit 1
fi
## ****************************************************************************



###############################################################################
#### App Execution Initialization ####
###############################################################################

## ****************************************************************************
## Execute any "init" commands passed to the GeneFlow CLI
CMD="${EXEC_INIT}"
echo "CMD=${CMD}"
safeRunCommand "${CMD}"
## ****************************************************************************



###############################################################################
#### Auto-Detect Execution Method ####
###############################################################################

# assign to new variable in order to auto-detect after Agave
# substitution of EXEC_METHOD
AUTO_EXEC=${EXEC_METHOD}
## ****************************************************************************
## Add app-specific paths to detect the execution method.
if [ "${EXEC_METHOD}" = "auto" ]; then
    # detect execution method
    if [ -f "${SCRIPT_DIR}/r-vcfr.simg" ]; then
        AUTO_EXEC=singularity
    else
        echo "Valid execution method not detected"
        echo
        usage
        exit 1
    fi
    echo "Detected Execution Method: ${AUTO_EXEC}"
fi
## ****************************************************************************



###############################################################################
#### App Execution Preparation, Common to all Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to prepare environment for execution
MNT=""; ARG=""; CMD0="mkdir -p ${LOG_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
## ****************************************************************************



###############################################################################
#### App Execution, Specific to each Exec Method ####
###############################################################################

## ****************************************************************************
## Add logic to execute app
## There should be one case statement for each item in $exec_methods
case "${AUTO_EXEC}" in
    singularity)
        MNT=""; ARG=""; MNT="${MNT} -B "; MNT="${MNT}\"${SCRIPT_DIR}/script._DIR}:/data1\""; ARG="${ARG} \"/data1/${SCRIPT_DIR}/script._BASE}\""; MNT="${MNT} -B "; MNT="${MNT}\"${INPUT_VCF_DIR}:/data2\""; ARG="${ARG} \"/data2/${INPUT_VCF_BASE}\""; MNT="${MNT} -B "; MNT="${MNT}\"${INPUT_FA_DIR}:/data3\""; ARG="${ARG} \"/data3/${INPUT_FA_BASE}\""; ARG="${ARG} \"${OUTPUT_DIR}/${OUTPUT_BASE}\""; ARG="${ARG} \"${MIN_QUAL}\""; ARG="${ARG} \"${MIN_DP}\""; ARG="${ARG} \"${MAX_DP}\""; ARG="${ARG} \"${MIN_MQ}\""; ARG="${ARG} \"${MAX_MQ}\""; CMD0="singularity -s exec ${MNT} ${SCRIPT_DIR}/r-vcfr.simg Rscript ${ARG}"; CMD0="${CMD0} >\"${LOG_FULL}/log.stdout\""; CMD0="${CMD0} 2>\"${LOG_FULL}/log.stderr\""; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
esac
## ****************************************************************************



###############################################################################
#### Cleanup, Common to All Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to cleanup execution artifacts, if necessary
## ****************************************************************************

